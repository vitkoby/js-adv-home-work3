'use strict';

//Ответы на вопросы: 
// 1. Позволяет разбивать объекты, массивы, функции на части для более удобного взаимодействия с ними, также
//  можно перезаписывать значения ключей

// Задача 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const mainClients = [...clients1, ...clients2];

function resArr(arr) {
    return arr.filter((e, i, a) => a.indexOf(e) == i);
}

// или

const anotherOne = [...new Set(mainClients)];

console.log(resArr(mainClients));
console.log(anotherOne);

// Задача 2

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const charactersShortInfo = characters.map(character => {
    return {
        firstName: character.name,
        lastName: character.lastName,
        age: character.age
    };
});

console.log(charactersShortInfo);

// Задача 3 

const user1 = {
    name: "John",
    years: 30
};

const { name, years, isAdmin = false } = user1;

const user = {
    name: user1.name,
    years: user1.years,
    ...(isAdmin)
};

console.log(user);

// Задача 4

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
};

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
};

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
};

const resSatushi = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log(resSatushi);

// Задача 5

const books = [
    {
        name: 'Harry Potter',
        author: 'J.K. Rowling'
    },
    {
        name: 'Lord of the rings',
        author: 'J.R.R. Tolkien'
    },
    {
        name: 'The witcher',
        author: 'Andrzej Sapkowski'
    }];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
};

const { name: bookName, author } = books;

const newBooks = books.concat({ name: 'Game of thrones', author: 'George R. R. Martin' });
console.log(newBooks);

// Задача 6 

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
};

employee.age = 30;
employee.salary = 1000;

const {name: someName, surname, age, salary} = employee;
const newEmployee = {...employee};

console.log(newEmployee);

// Задача 7

const array = ['value', () => 'showValue'];

const value = array[0];
const showValue = array[1]();

alert(value);
alert(showValue);












